// window.location.search returns the query string part of the URL
// console.log(window.location.search);

// instantiate a URLSearchParams object so we can execute methods to access the specific parts of the query string
let params = new URLSearchParams(window.location.search);
// console.log(params);

// spread the values to sey the key-value pairs of the object URLSearchParams
// console.log(...courseId);

// the has method checks if the courseId key exists in the URL query string
// true means that the key exists
// console.log(params.has('courseId'));

// get method returns the value of the key passed in as an argument
// console.log(params.get('courseId'));
let courseId = params.get('courseId');
// console.log(courseId);

let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let description = document.querySelector("#courseDescription")

fetch(`https://stark-woodland-77842.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	// console.log(data)

	// assign the current values as placeholders
	name.placeholder = data.name
	price.placeholder = data.price
	description.placeholder = data.description


	let editForm = document.querySelector('#editCourse');

	editForm.addEventListener("submit", (e) => {
		e.preventDefault();
			
		let token = localStorage.getItem("token");

		let courseName = document.querySelector('#courseName').value;
		let courseDescription = document.querySelector('#courseDescription').value;
		let coursePrice = document.querySelector('#coursePrice').value;
		
		fetch('https://stark-woodland-77842.herokuapp.com/api/courses', {
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${token}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				courseId: courseId,
				name: courseName,
				description: courseDescription,
				price: coursePrice
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);
			//creation of new course successful
			if(data === true) {
				Swal.fire({
				  title: 'Updated Successfully!',	
				  text: 'The course is now updated.',
				  icon: 'success',
				  confirmButtonText: 'OK'
				}).then (function(){
				//redirect to courses index page
				window.location.replace("courses.html")
				});
			} else {
				//error in creating course, redirect to error page
				Swal.fire({
				  title: 'Something went wrong.',	
				  icon: 'error',
				  confirmButtonText: 'OK'
				})
			}
		})
	})
})