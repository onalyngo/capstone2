let navItems = document.querySelector("#navSession-index");

let userToken = localStorage.getItem("token");
// console.log(userToken);

if(!userToken) {
	navItems.innerHTML =
		`
			<li class="navbar-nav ml-auto">
				<a href="./pages/login.html" class="nav-link"> Login </a>
				<a href="./pages/register.html" class="nav-link"> Register </a>
			</li>
		`
} else {
	navItems.innerHTML = 
		`
			<li class="navbar-nav ml-auto">
				<a href="./pages/profile.html" class="nav-link"> Profile </a>
				<a href="./pages/logout.html" class="nav-link"> Logout </a>
			</li>
		`
}