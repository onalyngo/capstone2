let token = localStorage.getItem("token")
// console.log(token);
let adminUser = localStorage.getItem("isAdmin");
let profileContainer = document.querySelector('#profileContainer');


if(!token || token === null){
	alert("You must login first.");
	window.location.replace("login.html");
} else {

	fetch('https://stark-woodland-77842.herokuapp.com/api/users/details', {
		headers: {
			'Authorization': `Bearer ${token}`
		},
	})
	.then(res => res.json())
	.then(data => {
		// console.log(data.enrollments);

		if(adminUser === "false" || !adminUser) {

		profileContainer.innerHTML =
            `
	            <div class="col-md-12">
					<section class="jumbotron">
						<table class="profDetails">
							<thead>
								<td>Name:</td>
								<th>${data.firstName} ${data.lastName}</th>
							<thead>
							<thead>
								<td>Email:</td>
								<th>${data.email}</th>
							<thead>
							<thead>
								<td>Mobile number:</td>
								<th>${data.mobileNo}</th>
							<thead>
						</table>
						<h3 class="text-center">Class History</h3>
						<table class="table">
							<thead>
								<tr class="table-active">
									<th> Course Name </th>
									<th> Enrolled Date </th>
									<th> Status </th>
								</tr>
							</thead>
								<tbody id="courseContainer">
								</tbody>
						</table>
					</section>
				</div>
			`
		courseContainer = document.querySelector("#courseContainer");
		// console.log(courseContainer);

		data.enrollments.forEach(enrollment => {
			let courseStatus = enrollment.status;
			// console.log(courseStatus);
			let date = new Date(enrollment.enrolledOn);
			// console.log(date)
			let year = date.getFullYear();
			// console.log(year);
			let month = date.getMonth() + 1;
			// console.log(date.getMonth);
			let day = date.getDate();
			console.log(day);

			if (day < 10){
				day = '0' + day;
			}
			if (month < 10){
				month = '0' + month;
			}
			let formatDate =  month + '-' + day + '-' + year;

			fetch(`https://stark-woodland-77842.herokuapp.com/api/courses/${enrollment.courseId}`)
			.then(res => res.json())
			.then(data => {
				// console.log(data.name)
				courseContainer.innerHTML +=
				`
					<tr>
					   <td>${data.name}</td> 
					   <td>${formatDate}</td> 
					   <td>${courseStatus}</td> 
					</tr> 
				`
			})
		})
	} else {
		profileContainer.innerHTML =
            `
	            <div class="col-md-12">
					<section class="jumbotron">
						<table class="profDetails">
							<thead>
								<td>Name:</td>
								<th>${data.firstName} ${data.lastName}</th>
							<thead>
							<thead>
								<td>Email:</td>
								<th>${data.email}</th>
							<thead>
							<thead>
								<td>Mobile number:</td>
								<th>${data.mobileNo}</th>
							<thead>
						</table>
					</section>
				</div>
			`
	}

	})
}

// Edit Profile Button
let editProfile = document.querySelector('#editProfleButton');

	editProfile.innerHTML =
		`
			<div class="col-md-2 offset-md-10">
				<a href="./editProfile.html" class="btn btn-block btn-primary"> Edit Profile </a>
			</div>
		`