let formSubmit = document.querySelector('#createCourse');

formSubmit.addEventListener("submit", (e) => {

	e.preventDefault()

	let courseName = document.querySelector("#courseName").value;
	let courseDescription = document.querySelector("#courseDescription").value;
	let coursePrice = document.querySelector("#coursePrice").value;

	let token = localStorage.getItem("token");

	fetch('https://stark-woodland-77842.herokuapp.com/api/courses', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify ({
			name: courseName,
			description: courseDescription,
			price: coursePrice
		})
	})
	.then(res => res.json())
	.then(data => {

		// console.log(body);
		if(data === true) {
			Swal.fire({
			  title: 'New course is added!',	
			  icon: 'success',
			  confirmButtonText: 'OK'
			}).then (function(){
			window.location.replace("courses.html")
			});
		} else {
			Swal.fire({
			  text: 'Something went wrong.',	
			  icon: 'error',
			  confirmButtonText: 'OK'
			})
		}
	})
})