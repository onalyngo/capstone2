let navItems = document.querySelector("#navSession");

// localStorage = an object used to store information in our clients/devices
let userToken = localStorage.getItem("token");
// console.log(userToken);

if(!userToken) {
	navItems.innerHTML =
		`
			<li class="navbar-nav ml-auto">
				<a href="./login.html" class="nav-link"> Login </a>
				<a href="./register.html" class="nav-link"> Register </a>
			</li>
		`
} else {
	navItems.innerHTML = 
		`
			<li class="navbar-nav ml-auto">
				<a href="./profile.html" class="nav-link"> Profile </a>
				<a href="./logout.html" class="nav-link"> Logout </a>
			</li>
		`
}