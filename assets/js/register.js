let registerForm = document.querySelector('#registerUser');

registerForm.addEventListener("submit", (e) => {
	// prevet the form to revert to it's default/blank values
	e.preventDefault();

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let userEmail = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNumber.length === 11)){

		// fetch('url', {options})
		// to process a request
		fetch('https://stark-woodland-77842.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: userEmail
				// email="onalyn@gmail.com"
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);

			if (data === false){

				fetch('https://stark-woodland-77842.herokuapp.com/api/users', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: userEmail,
						password: password1,
						mobileNo: mobileNumber
					})
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data)

					if(data === true){
						Swal.fire({
						  title: 'Registered Successfully!',	
						  text: 'You may now login.',
						  icon: 'success',
						  confirmButtonText: 'OK'
						}).then (function(){
						window.location.replace("login.html")
						});
					} else {
						Swal.fire({
						  title: 'Something went wrong.',	
						  icon: 'error',
						  confirmButtonText: 'OK'
						})
					}
				})
			} else {
				Swal.fire({	
				  text: 'Email has been used. Try a different one.',
				  icon: 'error',
				  confirmButtonText: 'OK'
				})
			}
		})
	} else {
		Swal.fire({	
		  text: 'Something went wrong.',
		  icon: 'error',
		  confirmButtonText: 'OK'
		})
	}
})