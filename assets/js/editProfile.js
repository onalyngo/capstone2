let adminUser = localStorage.getItem("isAdmin");
	// console.log(adminUser);
let userId = localStorage.getItem("id");
// console.log(userId)
let token = localStorage.getItem("token");
	// console.log(token);

fetch('https://stark-woodland-77842.herokuapp.com/api/users/details', {
		headers: {
			'Authorization': `Bearer ${token}`
		},
	})
	.then(res => res.json())
	.then(data => {
		// console.log(data);

	// assign the current values as placeholders
	firstName.placeholder = data.firstName
	lastName.placeholder = data.lastName
	contactNumber.placeholder = data.mobileNo

	let editProfile = document.querySelector('#editProfile');

	editProfile.addEventListener("submit", (e) => {
		e.preventDefault();
			
		let firstName = document.querySelector('#firstName').value;
		let lastName = document.querySelector('#lastName').value;
		let contactNumber = document.querySelector('#contactNumber').value;
		
		fetch('https://stark-woodland-77842.herokuapp.com/api/users', {
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${token}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				userId: userId,
				firstName: firstName,
				lastName: lastName,
				mobileNo: contactNumber,
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);
			if(data === true) {
				Swal.fire({
				  title: 'Updated Successfully!',	
				  text: 'Your profile is now updated.',
				  icon: 'success',
				  confirmButtonText: 'OK'
				}).then (function(){
				window.location.replace("profile.html")
				});
			} else {
				Swal.fire({
				  title: 'Something went wrong.',	
				  icon: 'error',
				  confirmButtonText: 'OK'
				})
			}
		})
	})
})