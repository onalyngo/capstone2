let loginForm = document.querySelector('#logInUser');

loginForm.addEventListener("submit", (e) => {	
	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;
	/*console.log(email);
	console.log(password);*/

	if(email == "" || password == "" ){
		Swal.fire({
			text: 'Please input your email and/or password',
			icon: 'error',
			confirmButtonText: 'OK'
		})
		// alert("Please input your email and/or password");
	} else {

		fetch('https://stark-woodland-77842.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);

			if (data.accessToken){
				// store the token in local storage
				localStorage.setItem('token', data.accessToken)
				/*
				localStorage {
					token: data.accessToken
				}
				*/

				// fetch request to get the details of the user
				fetch('https://stark-woodland-77842.herokuapp.com/api/users/details', {
					headers: {
						'Authorization': `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data);

					// store the user details in the local storage for future use
					localStorage.setItem('id', data._id);
					localStorage.setItem('isAdmin', data.isAdmin);

					// redirect the user to our courses page
					Swal.fire({
						  title: 'Login Successful!',	
						  icon: 'success',
						  confirmButtonText: 'OK'
						}).then (function(){
						window.location.replace("courses.html")
					});
				})
			} else {
				Swal.fire({
					text: 'Something went wrong.',
					icon: 'error',
					confirmButtonText: 'OK'
				})
			}
		})
	}

})