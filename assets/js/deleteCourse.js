// console.log(window.location.search);

let params = new URLSearchParams(window.location.search);
// console.log(params);
// console.log(params.has('courseId'));
let courseId = params.get('courseId');
// console.log(courseId);
let token = localStorage.getItem("token");
// let archiveCourse = document.querySelector('#archiveCourse');

/*archiveCourse.addEventListener("submit", (e) => {
	e.preve
})*/
fetch(`https://stark-woodland-77842.herokuapp.com/api/courses/${courseId}`, {
	method: 'DELETE',
	headers: {
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	if(data === true) {
		window.location.replace('./courses.html')
	} else {
		alert('Something went wrong.')
	}
})
