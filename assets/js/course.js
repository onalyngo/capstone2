// console.log(window.location.search);

let adminUser = localStorage.getItem("isAdmin");
// console.log(adminUser);

// instantiate a URLSearchParams object so we can execute methods to access the parameters
let params = new URLSearchParams(window.location.search);

// console.log(params);
// console.log(params.has('courseId'));
// console.log(params.get('courseId'));

let courseId = params.get('courseId');

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
let viewEnrollees = document.querySelector("#viewEnrollees");
let token = localStorage.getItem('token');

fetch(`https://stark-woodland-77842.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	// console.log(data);

	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;

	if(adminUser === "false" || !adminUser) {

		enrollContainer.innerHTML =
			`
				<button id="enrollButton" class="btn btn-block btn-primary"> Enroll </button>
				<a href="./courses.html" class="btn btn-info text-white btn-block"> Go Back </a>
			`
		document.querySelector("#enrollButton").addEventListener("click", () => {

			fetch('https://stark-woodland-77842.herokuapp.com/api/users/enroll', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId
				})
			})
			.then(res => res.json())
			.then(data => {
				// console.log(data);

				if(data === true) {
					Swal.fire({
					  title: 'Congratulation!',	
					  text: 'Thank you for enrolling. See you in class!',
					  icon: 'success',
					  confirmButtonText: 'OK'
					}).then (function(){
						window.location.replace("courses.html")
					});
				} else {
					Swal.fire({
					  text: 'Something went wrong.',
					  icon: 'error',
					  confirmButtonText: 'OK'
					});
					Swal.fire({
					  text: 'You must register first.',
					  icon: 'warning',
					  confirmButtonText: 'OK'
					}).then (function(){
						window.location.replace("register.html")
					});
				}
			})
		})
	} 

	// for admin view
	else {

		viewEnrollees.innerHTML +=
			`
				<div>
					<section class="jumbotron">
						<h3 class="text-center"> Enrollees </h3>
						<table class="table table-hover">
							<thead>
								<tr class="table-active">
									<th>Student Name</th>
									<th>Enrolled Date</th>
								</tr>
							</thead>
								<tbody id="enrolleesData">
								</tbody>
						</table>
					</section>
				</div>
			`

		enrolleesDetails = document.querySelector("#enrolleesData");
		// console.log(enrolleesData)

		data.enrollees.map(enrolleesData => {
			// console.log(enrolleesData)

			let date = new Date(enrolleesData.enrolledOn);
			// console.log(date)
			let year = date.getFullYear();
			// console.log(year);
			let month = date.getMonth() + 1;
			// console.log(date.getMonth);
			let day = date.getDate();
			// console.log(day);

			if (day < 10){
				day = '0' + day;
			}
			if (month < 10){
				month = '0' + month;
			}
			let formatDate =  month + '-' + day + '-' + year;

			fetch(`https://stark-woodland-77842.herokuapp.com/api/users/details/${enrolleesData.userId}`)
			.then(res => res.json())
			.then(data => {
				/*console.log(data);
				console.log(data.firstName + " " + data.lastName);*/

				enrolleesDetails.innerHTML +=
					`
					<tr>
					   <td>${data.firstName + " " + data.lastName}</td> 
					   <td>${formatDate}</td>
					</tr> 
					`
			})
		})
	}
})
